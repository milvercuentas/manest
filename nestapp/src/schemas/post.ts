import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type PostDocument = Post & Document;

@Schema()
export class Post {

  _id: number;

  @Prop()
  story_id: number;

  @Prop()
  title: string;

  @Prop()
  story_title: string;

  @Prop()
  author: string;

  @Prop({default: false})
  isDeleted: boolean;
}

export const PostSchema = SchemaFactory.createForClass(Post);
