import { HttpModule, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PostsModule } from './posts/posts.module';
import { ScheduleModule } from '@nestjs/schedule';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    HttpModule,
    PostsModule,
    ScheduleModule.forRoot(),
    MongooseModule.forRoot('mongodb://mongodb/nest')
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
