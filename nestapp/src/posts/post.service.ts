import { Injectable } from '@nestjs/common';
import { AlgoliaResponse } from './dto/algolia_dto';
import { InjectModel } from '@nestjs/mongoose';
import { Post, PostDocument } from '../schemas/post';
import { Model } from 'mongoose';

@Injectable()
export class PostService {

  constructor(@InjectModel(Post.name) private postModel: Model<PostDocument>) {}

  savePosts(posts: AlgoliaResponse): void {
    for (const response of posts.hits) {
      if (response.story_title || response.title) {
        this.postModel.findOne({story_id: response.story_id}, (err, res) => {
          if (res == null) {
            const createdPost = new this.postModel(response);
            createdPost.save();
          }
        });
      }
    }
  }

  async findAll(): Promise<Post[]> {
    return this.postModel.find({isDeleted: false}).exec();
  }

  async updatePost(post: Post): Promise<void> {
    const filter = { _id: post._id };
    const update = { isDeleted: true };

    await this.postModel.findOneAndUpdate(filter, update, {
      new: true,
      upsert: true
    });
    return null;
  }
}
