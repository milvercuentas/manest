import { Body, Controller, Get, Put } from '@nestjs/common';
import { Post } from '../schemas/post';
import { PostService } from './post.service';

@Controller('post')
export class PostController {

  constructor(private postService: PostService) {
  }

  @Get('posts')
  async getDelivery(): Promise<Post[]> {
    return this.postService.findAll();
  }

  @Put('posts')
  async update(@Body() post: Post): Promise<void> {
    return this.postService.updatePost(post);
  }
}
