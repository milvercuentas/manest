export class AlgoliaResponse {
  nbHits: number;
  params: string;
  hits: PostResponse[];
}

export class PostResponse {
  created_at: Date;
  title: string;
  url: string;
  author: string;
  points: string;
  story_text: string;
  comment_text: string;
  num_comments: string;
  story_id: number;
  story_title: string;
  story_url: string;
  parent_id: number;
  created_at_i: number;
}
