import { HttpService, Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { AxiosResponse } from 'axios';
import { AlgoliaResponse } from '../../dto/algolia_dto';
import { PostService } from '../../post.service';

@Injectable()
export class AlgoliaService {
  private readonly logger = new Logger(AlgoliaService.name);
  private readonly ALGOLIA_URL = 'https://hn.algolia.com/api/v1';

  constructor(private httpService: HttpService,
              private postService: PostService) {}

  @Cron('* 10 * * * *')
  handleCron() {
    this.logger.debug('Called when the current second is 45');
    this.retrievePosts();
  }

  private async retrievePosts() {
    await this.retrieveAlgoliaPosts('nodejs').then(value => {
      this.postService.savePosts(value.data);
    }).catch(reason => {
      console.log(reason);
    });
  }

  retrieveAlgoliaPosts(query: string): Promise<AxiosResponse<AlgoliaResponse>> {
    const params = {
      query: query,
      hitsPerPage: 1000
    }
    return this.httpService.get(`${this.ALGOLIA_URL}/search_by_date`, { params: params }).toPromise();
  }
}
