import { HttpModule, Module } from '@nestjs/common';
import { AlgoliaService } from './jobs/algolia/algolia.service';
import { PostService } from './post.service';
import { MongooseModule } from '@nestjs/mongoose';
import { Post, PostSchema } from '../schemas/post';
import { PostController } from './post.controller';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([{
      name: Post.name, schema: PostSchema
    }])
  ],
  controllers: [PostController],
  providers: [
    AlgoliaService,
    PostService],
})
export class PostsModule {}
