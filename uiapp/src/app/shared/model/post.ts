export class Post {
  isDeleted: boolean;
  _id: string;
  title: string;
  author: string;
  story_id: number;
  story_title: string;
}
