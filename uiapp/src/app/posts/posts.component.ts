import { Component, OnInit } from '@angular/core';
import {Post} from '../shared/model/post';
import {PostService} from '../shared/service/post.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {

  posts: Post[];
  constructor(private postService: PostService) {
    this.posts = [];
  }

  ngOnInit(): void {
    this.fetchPosts();
  }

  showTitle(post: Post): string {
    return post.story_title ? post.story_title : post.title;
  }

  deletePost(post: Post): void {
    this.postService.deletePost(post).subscribe(() => {
      this.fetchPosts();
    });
  }

  private fetchPosts(): void {
    this.postService.getPosts().subscribe(value => {
      this.posts = value;
    });
  }

}
